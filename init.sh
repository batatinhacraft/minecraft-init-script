# batatinhacraft setup script
#
# dropbox app session token
# D01cQ3PCJloAAAAAAAAAvGvdRjQQ6uKfpUQ1bYMIWIvvevRJQXaAeT3kBWJn-q25
# https://www.dropbox.com/oauth2/authorize?client_id=rgdlzfzz1ugcbma&response_type=code
# access token
# D01cQ3PCJloAAAAAAAAAvdTrDYAEZv5GW2mx2bp8GDg
# $ ./ngrok authtoken 1Rd5GjVvXvtmodoN83wR67uaI9x_76vgvjz6FvQ925XQHdDGj

init_gitlab () {
	json=$(gitlab create_project "$1" "{ description: '$2' }" --json)
	http_url_to_repo=$(echo $json | jq .result.http_url_to_repo)
	http_url_to_repo="${http_url_to_repo%\"}"
	http_url_to_repo="${http_url_to_repo#\"}"
	git remote add origin $http_url_to_repo
	git push -u origin master
}
init_git () {
	git init
	git add -A
	git commit -m "Initial commit"
}
init_npm () {
	npx license $(npm get init.license) -o "$(npm get init.author.name)" > LICENSE
	npx add-gitignore $@ node macOS windows linux sublimetext visualstudiocode
	npx covgen "$(npm get init.author.email)"
	npm init -y
}
mkdir "${1}" && cd $1
echo 'eula=true' > eula.txt
init_git
init_npm webstorm
git add eula.txt
git commit -m "Add eula" --allow-empty

heroku create $1
heroku git:remote -a $1
heroku buildpacks:add heroku/jvm -a $1
sleep 10

heroku buildpacks:add https://github.com/VastBlast/ns-heroku-minecraft -a $1
# heroku buildpacks:add https://github.com/davidnexuss/spigot-on-heroku -a $1
sleep 10

# heroku buildpacks:add https://github.com/kimbang012/Spigot-on-heroku -a $a
heroku config:set NGROK_API_TOKEN="1Rd5GjVvXvtmodoN83wR67uaI9x_76vgvjz6FvQ925XQHdDGj" -a $1
sleep 10

# heroku buildpacks:add https://github.com/jkutner/heroku-buildpack-minecraft -a $1
# heroku buildpacks:add https://github.com/VigroX/heroku-buildpack-mcserver -a $1
git commit -m "Build" --allow-empty
git push heroku master

heroku config:set DBCONFIG="D01cQ3PCJloAAAAAAAAAvdTrDYAEZv5GW2mx2bp8GDg" -a $1
heroku config:set DBCONFIG="D01cQ3PCJloAAAAAAAAAvGvdRjQQ6uKfpUQ1bYMIWIvvevRJQXaAeT3kBWJn-q25" -a $1
sleep 10

heroku config:set MINECRAFT_VERSION="1.8.8-R0.1-SNAPSHOT-latest" -a $1
sleep 10

git commit -m "Build" --allow-empty
git push heroku master

# init_gitlab "${1}" "Awesome minecraft server"
# git add .
# git commit -m "Docs updated" --allow-empty
# git push
heroku config:set NGROK_OPTS="--remote-addr 0.tcp.ngrok.io:17019" -a $1

# heroku dyno:restart -a $1
# heroku ps:restart -a $1

heroku buildpacks:add https://github.com/davidnexuss/spigot-on-heroku -a $a
git add .
git commit -m "Build" --allow-empty
git push heroku master
sleep 10

heroku ps:scale web=1 -a $1

heroku ps:exec -a $1

heroku open -a $1
heroku logs --tail -a $1
